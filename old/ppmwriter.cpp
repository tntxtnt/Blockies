#include <iostream>
#include <functional>
#include <string>
#include <vector>
#include <cstdlib>
#include <cmath>
#include <cstdio>

const char BLOCK = 219;
const int HALF_RAND_MAX = RAND_MAX / 2;

typedef std::vector<bool>            BlockiesDataRow;
typedef std::vector<BlockiesDataRow> BlockiesData;
typedef unsigned char uchar;
struct RGB {
    uchar r, g, b;
    RGB(uchar r, uchar g, uchar b) : r(r), g(g), b(b) {}
};

void createBlockies(const std::string&, int, int, int=1);
void createBlockies(const std::wstring&, int, int, int=1);

int main()
{
    createBlockies("INTP", 16, 16, 8);
    createBlockies("Trần Ngọc Trí", 16, 16, 8);
    createBlockies("tntxtnt", 16, 16, 8);
    createBlockies(L"Trần Ngọc Trí", 16, 16, 8);
}

BlockiesData createData(int w, int h)
{
    BlockiesDataRow dataRow(w, 0);
    BlockiesData data(h);
    for (int i = 0; i < h; ++i)
    {
        for (int j = 0; j < ceil(w/2.); ++j)
            dataRow[j] = dataRow[w-1-j] = rand() > HALF_RAND_MAX;
        data[i] = dataRow;
    }
    return data;
}

void writeP6Data(const BlockiesData& data, FILE* fout,
                 int w, int h, int scale, RGB color)
{
    fprintf(fout, "P6 %d %d %d ", w*scale, h*scale, 255);
    for (int i = 0; i < h; ++i)
        for (int j = 0; j < scale; ++j)
            for (int k = 0; k < w; ++k)
                for (int m = 0; m < scale; ++m)
                    if (data[i][k])
                        fprintf(fout, "%c%c%c", color.r, color.g, color.b);
                    else
                        fprintf(fout, "%c%c%c", 0, 0, 0);
}

void createBlockies(const std::string& s, int w, int h, int scale)
{
    srand(std::hash<std::string>()(s));

    BlockiesData data = createData(w, h);

    std::string fname = s + ".ppm";
    FILE* fout = fopen(fname.c_str(), "w");
    writeP6Data(data, fout, w, h, scale, RGB(rand(), rand(), rand()));
    fclose(fout);
}

void createBlockies(const std::wstring& s, int w, int h, int scale)
{
    srand(std::hash<std::wstring>()(s));

    BlockiesData data = createData(w, h);

    std::wstring fname = s + L".ppm";
    FILE* fout = _wfopen(fname.c_str(), L"w");
    writeP6Data(data, fout, w, h, scale, RGB(rand(), rand(), rand()));
    fclose(fout);
}
