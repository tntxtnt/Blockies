# Blockies

Generate PNG avatar (128x128) from a string.

![Blockies.png](img/Blockies.png)  
Blockies

![tntxtnt.png](img/tntxtnt.png)  
tntxtnt

![tntxtnt.png](img/helloworld.png)  
helloworld

## Build
`g++ -std=c++1z -O3 -Wall pngwriter.cpp lodepng.cpp -s -o blockies`

## Run
`blockies <string>`

## License
MIT
