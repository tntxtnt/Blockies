#include <iostream>
#include <vector>
#include <string>
#include <random>
#include <algorithm>
#include "lodepng.h"

using BoolVec = std::vector<bool>;
using BoolMat = std::vector<BoolVec>;

struct Color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
    Color(uint8_t r=0, uint8_t g=0, uint8_t b=0, uint8_t a=255) :
        r{r}, g{g}, b{b}, a{a} { }
};
struct RawImage {
    int width;
    int height;
    std::vector<uint8_t> data;
    RawImage(int w, int h) : width{w}, height{h}, data(4*w*h, 0) {}
    void setPixel(int x, int y, Color c)
    {
        uint8_t* ptr = &data[4*width*y + 4*x];
        ptr[0] = c.r;
        ptr[1] = c.g;
        ptr[2] = c.b;
        ptr[3] = c.a;
    }
    Color getPixel(int x, int y)const
    {
        const uint8_t* ptr = &data[4*width*y + 4*x];
        return {ptr[0], ptr[1], ptr[2], ptr[3]};
    }
};

BoolMat vSymmetry(int width, int height, std::mt19937&);
void print01(const BoolMat&);
void print219(const BoolMat&);
void fillCell(RawImage&, int, int, int, int, Color);

int main(int argc, char** argv)
{
    std::string name;
    if (argc > 1)
    {
        name = argv[1];
    }
    else
    {
        std::cout << "Your name: ";
        std::getline(std::cin, name);
    }

    auto seed = std::hash<std::string>{}(name);
    std::cout << "Your seed: " << seed << "\n";
    auto prng = std::mt19937(seed);

    int dataW = 16;
    int dataH = 16;
    auto bitData = vSymmetry(dataW, dataH, prng);
    print219(bitData);

    int imageW = 128;
    int imageH = 128;
    RawImage image(imageW, imageH);
    Color fillColor;
    fillColor.r = std::uniform_int_distribution<unsigned char>{0,255}(prng);
    fillColor.g = std::uniform_int_distribution<unsigned char>{0,255}(prng);
    fillColor.b = std::uniform_int_distribution<unsigned char>{0,255}(prng);
    int cellW = imageW/dataW;
    int cellH = imageH/dataH;
    for (size_t y = 0; y < bitData.size(); ++y)
        for (size_t x = 0; x < bitData[y].size(); ++x)
            fillCell(image, cellW, cellH, x, y,
                     bitData[y][x] ? fillColor : Color());
    lodepng::encode((name + ".png").c_str(), image.data, imageW, imageH);
}



void fillCell(RawImage& img, int cellW, int cellH, int posX, int posY,
              Color color)
{
    for (int y = posY*cellH; y < posY*cellH + cellH; ++y)
        for (int x = posX*cellH; x < posX*cellW + cellW; ++x)
            img.setPixel(x, y, color);
}

BoolMat vSymmetry(int width, int height, std::mt19937& prng)
{
    // Generate data, vertical line symmetry
    BoolMat bitData(height, BoolVec(width));
    for (auto& row : bitData)
    {
        std::generate_n(begin(row), width/2, [&prng](){
            return std::uniform_int_distribution<int>{0,1}(prng);
        });
        std::copy_n(begin(row), width/2, row.rbegin());
    }
    return bitData;
}

void print01(const BoolMat& a)
{
    for (auto& row : a)
    {
        for (bool b : row) std::cout << b << " ";
        std::cout << "\n";
    }
}

void print219(const BoolMat& a)
{
    static std::string BLOCK = std::string(2, 219);
    for (auto& row : a)
    {
        for (bool b : row) std::cout << (b ? BLOCK : "  ");
        std::cout << "\n";
    }
}
